#!/usr/bin/env bash

usage() {
    echo "Usage:"
    echo "$0 node-name CIDR"
}

if [[ -z $1 ]] || [[ -z $2 ]] ;
then
    usage
    exit 1
fi
/nebula/nebula-cert sign -name $1 -ip $2
