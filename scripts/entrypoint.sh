#!/usr/bin/env bash

set -e


if [[ -z $1 ]];
then
    /nebula/nebula -config /nebula/config.yaml
else
    if [[ "$1" == "cert-auth" ]];
    then
        if [[ -z $2 ]];
        then
            echo "Org name needed for cert auth creation"
            exit 1
        fi
        /nebula/scripts/create-cert-auth.sh $2
    elif [[ "$1" == "sign" ]];
    then
        if [[ -z $2 ]] || [[ -z $3 ]];
        then
            echo "Name and CIDR are needed"
            exit 1
        fi
        /nebula/scripts/sign-for-zone.sh $2 $3

    else
        exec "$@"
    fi
fi
