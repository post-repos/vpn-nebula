#!/usr/bin/env bash

NAME=${ORG_NAME}
if [[ ! -z $1 ]];
then
    NAME=$1
fi

/nebula/nebula-cert ca -name ${NAME}
