# NEBULA VPN on docker

## Build it

``` shell
docker build -t juanmatias/vpn-nebula:0 .
```

## Steps to create a VPN

Chose a working dir, here you will get all the files.

Create the VPN Certificate

``` shell
docker run  --rm -it -v $(pwd):/project juanmatias/vpn-nebula:0 cert-auth myOrg
```

Set the org name you want

Check files

``` shell
$ ls
ca.crt  ca.key

```

Store the ca.key in a safe place!

Find the CIDRs you want to use for your zones. (check https://github.com/slackhq/nebula#4-nebula-host-keys-and-certificates-generated-from-that-certificate-authority)

Sign certs for zones

``` shell
docker run  --rm -it -v $(pwd):/project juanmatias/vpn-nebula:0 sign lighthouse 192.168.90.1/24
docker run  --rm -it -v $(pwd):/project juanmatias/vpn-nebula:0 sign laptop 192.168.90.2/24 -groups "laptop,home,ssh"
```

Check files

``` shell
$ ls
ca.crt  ca.key  laptop.crt  laptop.key lighthouse.crt  lighthouse.key

```

### Create the lighthouse

Create a `lighthouse` directory and copy the lighthouse config and crt/key files there:

``` shell
mkdir lighthouse
cp {source}/lighthouse.yaml lighthouse
mv lighthouse.* lighthouse
cp ca.crt lighthouse
cd lighthouse
```

DO NOT COPY ca.key TO INDIVIDUAL NODES.

Edit the config file, under `static_host_map:` add the lighthouse inner IP with the public address. E.g.

``` yaml
static_host_map:
 # how to find one or more lighthouse nodes
 # you do NOT need every node to be listed here!
 #
 # format "Nebula IP": ["public IP or hostname:port"]
 # 
 "192.168.90.1": ["kungfoo.com:4242"]
```

We are using `kungfoo.com` as a fake address, for sake of this test add it to your `/etc/host`.

Now, run the lighthouse.

``` shell
docker run  --rm -it -p 4242:4242/udp --privileged  --device=/dev/net/tun -v $(pwd):/project -v $(pwd)/lighthouse.yaml:/nebula/config.yaml --add-host "kungfoo.com:127.0.0.1"  --name lighthouse juanmatias/vpn-nebula:0
```

Note here we are using the `add-host` flag since this is a fake domain and it is served nowhere.


### Create the first node

For this example, we will create a node inside the same host, in a new container.

Create a directory called `laptop` and copy there the config and crt/key files:

``` shell
mkdir laptop
cp {source}/node.yaml laptop
mv laptop.* laptop
cp ca.crt laptop
cd laptop
```

Edit the config file adding the lighthouse data:

``` yaml
static_host_map:
 # how to find one or more lighthouse nodes
 # you do NOT need every node to be listed here!
 #
 # format "Nebula IP": ["public IP or hostname:port"]
 # 
 "192.168.90.1": ["kungfoo.com:4242"]

lighthouse:
  interval: 60

  # if you're a lighthouse, say you're a lighthouse
  #
  am_lighthouse: false

  hosts:
    # If you're a lighthouse, this section should be EMPTY
    # or commented out. If you're NOT a lighthouse, list 
    # lighthouse nodes here, one per line, in the following
    # format:
    #
    - "192.168.90.1"
```

...and the right crt/key files:

``` yaml
pki:
  # every node needs a copy of the CA certificate,
  # and its own certificate and key, ONLY.
  #
  ca: /project/ca.crt
  cert: /project/laptop.crt
  key: /project/laptop.key
```

Now get the IP the lighthouse is using:

``` shell
docker inspect lighthouse | jq '.[0].NetworkSettings.IPAddress'
```

And use it for starting the `laptop` node:

``` shell
docker run  --rm -it --privileged  --device=/dev/net/tun -v $(pwd):/project -v $(pwd)/node.yaml:/nebula/config.yaml --add-host "kungfoo.com:172.17.0.2" --name laptop debian:10
```

Go to "Install and run Nebula" and come back here.

#### Test it

Access the container:

``` shell
docker exec -it laptop bash
```

And get the IPs

``` shell
root@48561daf22e4:/# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
3: nebula1: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1300 qdisc fq_codel state UNKNOWN group default qlen 500
    link/none 
    inet 192.168.90.2/24 scope global nebula1
       valid_lft forever preferred_lft forever
48: eth0@if49: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.3/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever

```

The eth0 address could change, but take a look at the nebula1 interface... we are in the VPN.

### A sample internet web server

Ok, but why we are doing this? 

Honestly, I don't know.... but let's play a little.

Let's create an `nginx` server.

Create the creds. In the project dir (where the ca.crt and ca.key are):

``` shell
docker run  --rm -it -v $(pwd):/project juanmatias/vpn-nebula:0 sign nginx1 192.168.90.3/24 -groups "servers"
```

These files are created: `nginx1.crt` and `nginx1.key`

Create the directory to have it all isolated and copy the  needed files:

``` shell
mkdir nginx1
cp {source}/node.yaml nginx1
mv nginx1.* nginx1
cp ca.crt nginx1
cd nginx1
```

Edit the config file adding the lighthouse data:

``` yaml
static_host_map:
 # how to find one or more lighthouse nodes
 # you do NOT need every node to be listed here!
 #
 # format "Nebula IP": ["public IP or hostname:port"]
 # 
 "192.168.90.1": ["kungfoo.com:4242"]

lighthouse:
  interval: 60

  # if you're a lighthouse, say you're a lighthouse
  #
  am_lighthouse: false

  hosts:
    # If you're a lighthouse, this section should be EMPTY
    # or commented out. If you're NOT a lighthouse, list 
    # lighthouse nodes here, one per line, in the following
    # format:
    #
    - "192.168.90.1"
```

...and the right crt/key files:

``` yaml
pki:
  # every node needs a copy of the CA certificate,
  # and its own certificate and key, ONLY.
  #
  ca: /project/ca.crt
  cert: /project/laptop.crt
  key: /project/laptop.key
```

With the lighthouse IP start the `nginx1` node:

``` shell
docker run  --rm -it --privileged  --device=/dev/net/tun -v $(pwd):/project -v $(pwd)/node.yaml:/nebula/config.yaml --add-host "kungfoo.com:172.17.0.2" --name nginx1 nginx:1.23.4-bullseye
```

Go to "Install and run Nebula" and come back here.

#### Test it

Try to access the nginx server from the laptop using the VPN IP.

``` shell
$ docker exec -it laptop curl 192.168.90.3
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

Come on, dude! We did it!

### A foreign node

Ok, these nodes are all running in docker in the same host... well, let's try something else.

Get a different machine (PC, laptop, whatever you want).

I got a Manjaro laptop, let's have our hands dirty...

Create the creds. In the project dir (where the ca.crt and ca.key are):

``` shell
docker run  --rm -it -v $(pwd):/project juanmatias/vpn-nebula:0 sign thealien 192.168.90.4/24 -groups "laptop,home,ssh"
```

Copy `thealien.*`, `node.yaml` config file and ca.crt to the new laptop.

DO NOT COPY ca.key TO INDIVIDUAL NODES.

Download nebula:

``` shell
curl -sLO "https://github.com/slackhq/nebula/releases/download/v1.6.1/nebula-linux-amd64.tar.gz" && \
    tar -zxvf nebula-linux-amd64.tar.gz && \
    chmod ugo+x nebula*
```

Edit the config file adding the lighthouse data:

``` yaml
static_host_map:
 # how to find one or more lighthouse nodes
 # you do NOT need every node to be listed here!
 #
 # format "Nebula IP": ["public IP or hostname:port"]
 # 
 "192.168.90.1": ["kungfoo.com:4242"]

lighthouse:
  interval: 60

  # if you're a lighthouse, say you're a lighthouse
  #
  am_lighthouse: false

  hosts:
    # If you're a lighthouse, this section should be EMPTY
    # or commented out. If you're NOT a lighthouse, list 
    # lighthouse nodes here, one per line, in the following
    # format:
    #
    - "192.168.90.1"
```

...and the right crt/key files:

``` yaml
pki:
  # every node needs a copy of the CA certificate,
  # and its own certificate and key, ONLY.
  #
  ca: /project/ca.crt
  cert: /project/thealien.crt
  key: /project/thealien.key
```
Note you should set the right path here.

In this case, we need to add the URL to `/etc/hosts` file. Since we published the port in the host machine previously, get the first host machine IP and add it to the file:

``` shell
sudo bash -c "echo \"192.168.69.130 kungfoo.com\" >> /etc/hosts"
```

Note this IP is my case, check yours first.

Run nebula:

``` shell
sudo ./nebula -config ./node.yaml
```

Note here it is run with sudo since we need access to reserved devices.

#### Test it

Now open a new terminal and try to access the nginx server in the VPN IP.

``` shell
$ curl 192.168.90.3

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>

```

And it worked, pal!

#### Test it secure, the Alien machine

Let's say the developer using the alien laptop needs to access the nginx server for updating the default page. (which is a good idea after all)

First we should have SSH enabled in the nginx1 server.

So in the host access the container:

``` shell
docker exec -it nginx1 bash
```

And install ssh:

``` shell
apt install -y openssh-server
service ssh start
```

Now from the alien laptop:

``` shell
ssh 192.168.90.3
```

It should ask for validating the fingerprint.

But we have no user for going into the system.

So let's create one in the nginx1 server:

``` shell
apt install -y sudo && \
useradd --uid 1000 -m nebulatest && echo nebulatest ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/nebulatest && chmod 0440 /etc/sudoers.d/nebulatest
```

And update the password:

``` shell
passwd nebulatest
```

Now, back in the alien laptop, let's try it again:

``` shell
ssh nebulatest@192.168.90.3
```

Accept the fingerprint and enter the password. 

And we are in!

Let's play a little.

Edit the HTML on the SSH connection:

``` shell
sudo apt install -y vim
sudo vim /usr/share/nginx/html/index2.html
```
Note we are using sudo since here we have a non root user!

Make your changes and exit vim (if you can), and test it again, from the alient laptop (outside your ssh connection):

``` shell
$ curl 192.168.90.3
```

Your changes should be there!!!

## Install and run Nebula

Enter the container:

``` shell
docker exec -it containername bash

```

Inside run:

``` shell
cd /nebula
apt update && apt install -y curl
curl -sLO "https://github.com/slackhq/nebula/releases/download/v1.6.1/nebula-linux-amd64.tar.gz" && \
    tar -zxvf nebula-linux-amd64.tar.gz && \
    chmod ugo+x nebula*
```

And run nebula:

``` shell
/nebula/nebula -config /nebula/config.yaml
```
