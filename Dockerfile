FROM debian:11.6-slim

ARG ORG_NAME="Myorganization, Inc"
ARG NODE_NAME="lighthouse"
ARG USERNAME=nebula
ARG USER_UID=1000
ARG USER_GID=1000

ENV ORG_NAME=$ORG_NAME
ENV NODE_NAME=$NODE_NAME

RUN apt update && apt upgrade -y

RUN apt install -y \
    curl

WORKDIR /nebula

RUN curl -sLO "https://github.com/slackhq/nebula/releases/download/v1.6.1/nebula-linux-amd64.tar.gz" && \
    tar -zxvf nebula-linux-amd64.tar.gz && \
    chmod ugo+x nebula*

ADD scripts/ /nebula/scripts
ADD configs/ /nebula/configs

RUN chmod ugo+x /nebula/scripts/*

RUN if [[ "${NODE_NAME}" == "lighthouse" ]]; \
    then \
         cp /nebula/configs/lighthouse.yaml /nebula/config.yaml; \
    else \
         cp /nebula/configs/node.yaml /nebula/config.yaml; \
    fi

RUN mkdir /project && \
    chown root:root -R /nebula /project
#RUN mkdir /project && \
#    chown ${USER_UID}:${USER_GID} -R /nebula /project

#RUN groupadd --gid $USER_GID $USERNAME \
#    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME
#    #&& echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
#    #&& chmod 0440 /etc/sudoers.d/$USERNAME
#
#USER $USERNAME
WORKDIR /project


ENTRYPOINT ["/nebula/scripts/entrypoint.sh"]
